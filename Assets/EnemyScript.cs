﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {
	public GameObject explosion;

	Rigidbody2D rb2d;
	// Use this for initialization
	void Awake () {
		rb2d = GetComponent<Rigidbody2D> ();
	}
	
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.relativeVelocity.magnitude > 10) {
			Instantiate (explosion, transform.position, transform.rotation);
			Destroy (gameObject, 0.2f);
		}
	}
}
