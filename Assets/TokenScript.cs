﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TokenScript : MonoBehaviour, IGrammarToken {

	public GameObject[] variations;

	public void RandomResolve (){
		GameObject prefab = variations[Random.Range(0, variations.Length)];
		Instantiate (prefab, transform.position, transform.rotation, transform.parent);

		Destroy (gameObject);
	}
}
