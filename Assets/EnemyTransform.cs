﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGrammarToken {
	void RandomResolve();
}

public class EnemyTransform : MonoBehaviour {

	public Sprite[] sprites;

	void Start () {
		GetComponent<SpriteRenderer> ().sprite = sprites [Random.Range (0, sprites.Length)];
	}
}
