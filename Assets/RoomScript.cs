﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomScript : MonoBehaviour {

	/*public GameObject bridgePrefab;
	public Transform bridgeTop;
	public Transform bridgeSide;*/

	public void PlaceObjects(Cell cell) {
		GetComponent<SpriteRenderer> ().enabled = false;
		GameObject.Find ("RoomManager").GetComponent<RoomManager> ().DrawRoomStep1 (cell, transform.position);
	}
}
