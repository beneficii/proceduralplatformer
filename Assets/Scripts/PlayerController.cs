﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float maxSpeed = 3;
	public float jumpVelocity = 8;
	

	enum State {Iddle, Running, Jumping}
	State state = State.Iddle;
	enum Buff {None, Spring, Ladder}
	List<Buff> buffs;


	float direction = 0;
	Rigidbody2D rb2d;


	void Awake () {
		rb2d = GetComponent<Rigidbody2D> ();
		buffs = new List<Buff> ();
	}

	void Update () {
		direction = Input.GetAxis ("Horizontal");
		if (Input.GetButtonDown ("Jump")) {
			float yVelocity = rb2d.velocity.y;
			if (buffs.Contains (Buff.Spring)) {
				yVelocity = 15f;//TopPlatformVelocity ();
			} else {
				yVelocity = jumpVelocity;
			}

			//buffs.Remove (Buff.Ladder);
			rb2d.velocity = new Vector2 (rb2d.velocity.x, yVelocity);
		} else if (buffs.Contains(Buff.Ladder)) {
			if (Input.GetKey (KeyCode.W)) {
				rb2d.MovePosition ((Vector2)transform.position + Vector2.up * Time.deltaTime * 4f);
				rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
			} else if (Input.GetKey (KeyCode.S)) {
				rb2d.MovePosition ((Vector2)transform.position + Vector2.down * Time.deltaTime * 4f);
				rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
			}
		}
	}

	float TopPlatformVelocity() {
		RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 30f, LayerMask.GetMask("Platform"));
		if (hit.collider != null) {
			float height = Mathf.Abs(hit.point.y - transform.position.y);
			return Mathf.Sqrt((height+2)*20f);
		}

		return jumpVelocity;
	}

	void FixedUpdate () {
		rb2d.velocity = new Vector2 (direction*maxSpeed, rb2d.velocity.y);
	}

	void OnTriggerEnter2D(Collider2D other) {
		switch(other.tag) {
		case "Trampoline":
			buffs.Add (Buff.Spring);
			break;
		case "Ladder":
			buffs.Add (Buff.Ladder);
			//rb2d.velocity = new Vector2 (rb2d.velocity.x, 0);
			//rb2d.gravityScale = 0;
			break;
		}
	}

	void OnTriggerExit2D(Collider2D other){
		switch(other.tag) {
		case "Trampoline":
			buffs.Remove (Buff.Spring);
			break;
		case "Ladder":
			buffs.Remove (Buff.Ladder);
			if (!buffs.Contains (Buff.Ladder)) {
				//rb2d.gravityScale = 1;
			}
			break;
		}
	}
}
