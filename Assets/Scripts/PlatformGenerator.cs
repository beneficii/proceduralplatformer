﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformGenerator : MonoBehaviour {

	const int CellSize = 10;

	public Button btnNext;

	public GameObject roomPrefab;
	public GameObject bridgePrefab;

	public int width = 6;
	public int height = 4;

	public int seed = 626340;

	GameObject[] rooms;
	List<GameObject> bridges;
	BtRectangle maze;

	RoomManager roomManager;

	int step = 1;

	public void OnButtonNext() {
		switch(step) {
		case 1:
			StartCoroutine (MazeStep1 ());
			break;
		case 2:
			StartCoroutine (MazeStep2 ());
			break;
		case 3:
			StartCoroutine (MazeStep3 ());
			break;
		case 4:
			StartCoroutine (MazeStep4 ());
			break;

		}
	}

	void Awake() {
		roomManager = GameObject.Find ("RoomManager").GetComponent<RoomManager> ();
	}

	void AddRoom(int index, Vector3 pos) {
		rooms [index] = Instantiate<GameObject> (roomPrefab, transform.position + pos*CellSize, Quaternion.identity, transform);
	}

	IEnumerator MazeStep1() {
		btnNext.enabled = false;
		Random.InitState(seed);

		foreach (Transform t in transform) {
			Destroy (t.gameObject);
		}
		roomManager.ClearRooms ();

		bridges = new List<GameObject> ();
		rooms = new GameObject[width * height];
		AddRoom (0, transform.position);
		maze = new BtRectangle (width, height);
		yield return maze.Generate (DrawRoom, 0f);
		step = 2;
		btnNext.enabled = true;
	}

	IEnumerator MazeStep2() {
		btnNext.enabled = false;

		foreach (GameObject g in bridges) {
			Destroy (g);
		}

		yield return new WaitForSeconds (0.2f);

		for (int x = -CellSize/2; x <= CellSize * width - CellSize/2; x++) {
			roomManager.DrawTile (2, new Vector3(x, -CellSize/2));
		}

		for (int y = -CellSize/2 +1; y <= CellSize * height - CellSize/2; y++) {
			roomManager.DrawTile (4, new Vector3(-CellSize/2, y));
			roomManager.DrawTile (4, new Vector3(CellSize * width-CellSize/2, y));
		}

		yield return new WaitForSeconds (0.1f);

		for (int i = 0; i < rooms.Length; i++) {
			rooms [i].GetComponent<RoomScript> ().PlaceObjects (maze.GetCell(i));
			yield return new WaitForSeconds (0.1f);
		}
		step = 3;
		btnNext.enabled = true;
	}

	IEnumerator MazeStep3() {
		btnNext.enabled = false;

		foreach (GameObject g in GameObject.FindGameObjectsWithTag("Token")) {
			g.GetComponent<IGrammarToken> ().RandomResolve ();
			yield return null;
		}
		step = 4;
		btnNext.enabled = true;
	}

	IEnumerator MazeStep4() {
		btnNext.enabled = false;

		foreach (GameObject g in GameObject.FindGameObjectsWithTag("Token")) {
			g.GetComponent<IGrammarToken> ().RandomResolve ();
			yield return null;
		}
		//step = 1;
		//btnNext.enabled = true;
	}

	void DrawRoom(Int2 pos1, Int2 pos2, int index) {
		Vector2 v1 = transform.position + new Vector3 (pos1.x, pos1.y,0);
		Vector2 v2 = transform.position + new Vector3 (pos2.x, pos2.y,0);

		AddRoom (index, new Vector3 (pos2.x, pos2.y,0));
		bridges.Add (Instantiate<GameObject> (bridgePrefab, (v1 + v2) *CellSize/2, transform.rotation, transform));
	}
}
