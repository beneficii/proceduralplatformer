﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cell {
	static int unvisited = 0;
	bool visited;
	public bool Visited {
		get{return visited; }
		set{
			if (value != visited) {
				if (value == true) {
					unvisited--;
				} else {
					unvisited++;
				}
				visited = value;
			}
		}
	}

	public bool rightOpen;
	public bool topOpen;

	public Cell () {
		this.visited = false;
		this.rightOpen = false;
		this.topOpen = false;
		unvisited++;
	}

	public static bool AllVisited() {
		return unvisited == 0;
	}
}

public class Int2 {
	public int x;
	public int y;

	public Int2 (int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}


public abstract class BackTrackGenerator {

	protected int width;
	protected int height;

	Cell[] cells;
	int current;

	protected abstract Int2 GetPos (int index);
	protected abstract int GetIdx (int x, int y);
	protected abstract IEnumerable<int> Neighbours (int index);

	public delegate void DrawFunction(Int2 cur, Int2 next, int index);

	void DrawNext(int next, DrawFunction drawFunction) {
		Int2 pos1 = GetPos (current);
		Int2 pos2 = GetPos (next);

		if (pos2.x > pos1.x) cells[current].rightOpen = true;
		if (pos1.x > pos2.x) cells[next].rightOpen = true;
		if (pos2.y > pos1.y) cells[current].topOpen = true;
		if (pos1.y > pos2.y) cells[next].topOpen = true;

		drawFunction (pos1, pos2, next);
	}

	public BackTrackGenerator (int width, int height) {
		this.width = width;
		this.height = height;
	}

	void Visit(int index) {
		current = index;
		cells [index].Visited = true;
	}
	

	public IEnumerator Generate(DrawFunction drawFunction, float delay = 0.1f) {
		cells = new Cell[width*height];
		for (int i = 0; i < cells.Length;i++) {
			cells [i] = new Cell ();
		}
		Stack<int> stack = new Stack<int> ();

		Visit (0);
		while (!Cell.AllVisited ()) {
			List<int> nodes = new List<int> ();
			foreach(int i in Neighbours(current)) {
				if (!cells [i].Visited) {
					nodes.Add (i);
				}
			}
			if (nodes.Count > 0) {
				int next = nodes [Random.Range (0, nodes.Count)];
				stack.Push (current);
				//remove wall current->next
				DrawNext(next, drawFunction);
				yield return new WaitForSeconds(delay);
				Visit (next);
			} else if (stack.Count > 0) {
				current = stack.Pop ();
			}
			//yield return new WaitForSeconds(delay);
		}
	}

	public Cell GetCell(int index) {
		return cells [index];
	}

}


/*  ================ Steps ===================================
    Make the initial cell the current cell and mark it as visited
    While there are unvisited cells
        If the current cell has any neighbours which have not been visited
            Choose randomly one of the unvisited neighbours
            Push the current cell to the stack
            Remove the wall between the current cell and the chosen cell
            Make the chosen cell the current cell and mark it as visited
        Else if stack is not empty
            Pop a cell from the stack
            Make it the current cell
 */