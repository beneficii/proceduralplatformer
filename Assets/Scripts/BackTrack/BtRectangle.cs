﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BtRectangle : BackTrackGenerator {

	public BtRectangle (int width, int height) : base (width, height){}

	protected override Int2 GetPos (int index) {
		return new Int2 (index%width, index/width);
	}

	protected override int GetIdx (int x, int y) {
		return y * width + x;
	}

	protected override IEnumerable<int> Neighbours (int index) {
		Int2 pos = GetPos (index);

		if (pos.x > 0) yield return GetIdx (pos.x-1, pos.y);
		if (pos.y > 0) yield return GetIdx (pos.x, pos.y-1);
		if (pos.x < width-1) yield return GetIdx (pos.x+1, pos.y);
		if (pos.y < height-1) yield return GetIdx (pos.x, pos.y+1);
	}
}
