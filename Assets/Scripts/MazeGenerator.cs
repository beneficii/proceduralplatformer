﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeGenerator : MonoBehaviour {

	public GameObject tilePrefab;
	public GameObject doorPrefab;

	public int width = 6;
	public int height = 4;
	public float delay = 0.4f;

	void Awake() {
		BtRectangle maze = new BtRectangle (width, height);

		StartCoroutine (maze.Generate(DrawNext));
	}

	void DrawNext(Int2 pos1, Int2 pos2, int index) {
		Vector2 v1 = transform.position + new Vector3 (pos1.x, pos1.y,0);
		Vector2 v2 = transform.position + new Vector3 (pos2.x, pos2.y,0);

		//Instantiate (tilePrefab, v1, transform.rotation);
		Instantiate (tilePrefab, v2, transform.rotation, transform);

		Instantiate (doorPrefab, (v1+v2)/2, transform.rotation, transform);
	}
}
