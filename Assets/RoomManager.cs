﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour {
	public TextAsset[] roomsJSON; //0-topOpen, 1-topClosed, 2- sideOpen, 3- sideClosed;

	[System.Serializable]
	public class Pair {
		public int index;
		public GameObject tile;
	}

	public Pair[] tilePairs;
	Dictionary<int, GameObject> tiles;

	[System.Serializable]
	public class Layer {
		public int[] data;
		public int width;
		public int height;
		public string name;

		public IEnumerable<Vector3> LayerTiles() {
			for (int i = 0; i < data.Length; i++) {
				yield return new Vector3 (i%width, height - i/width, data[i]);
			}
		}
	}

	[System.Serializable]
	public class RoomTemplates {
		public List<Layer> layers; 
	} 

	List<List<Layer>> layers;

	void Awake() {
		tiles = new Dictionary<int, GameObject> ();

		for(int i = 0;i<tilePairs.Length;i++) {
			Pair pair = tilePairs[i];
			tiles.Add (pair.index, pair.tile);
		}

		layers = new List<List<Layer>> ();
		for (int i = 0; i < roomsJSON.Length; i++) {
			layers.Add (JsonUtility.FromJson<RoomTemplates> (roomsJSON[i].text).layers);
		}
		Layer commonLayer = new Layer ();
		commonLayer.width = 4;
		commonLayer.height = 1;
		commonLayer.data = new int[] { 2, 2, 2, 2 };
		layers.Add (new List<Layer> (new [] {commonLayer}));
	}

	public enum LayerType{TopOpen = 0, TopClosed = 1, sideOpen = 2, sideClosed = 3, Common = 4};

	public void DrawTile(int type, Vector3 pos) {
		Instantiate (tiles[type], pos, Quaternion.identity, transform);
	}

	void DrawRandomLayer(LayerType type, Vector3 pos) {
		List<Layer> layerSet = layers[(int)type];
		Layer layer = layerSet[Random.Range (0, layerSet.Count)];

		foreach (Vector3 tile in layer.LayerTiles()) {
			DrawTile ((int)tile.z, new Vector3 (pos.x + tile.x, pos.y + tile.y, 0));
		}
	}

	public void DrawRoomStep1(Cell cell, Vector3 startPos) {
		Vector2 pos = (Vector2)startPos;

		Vector2 topPosition = pos + new Vector2 (-5, -5);
		if (cell.topOpen) {
			DrawRandomLayer (LayerType.TopOpen, topPosition);
		} else {
			DrawRandomLayer (LayerType.TopClosed, topPosition);
		}

		Vector2 sidePosition = pos + new Vector2 (1, -5);
		if (cell.rightOpen) {
			DrawRandomLayer (LayerType.sideOpen, sidePosition);
		} else {
			DrawRandomLayer (LayerType.sideClosed, sidePosition);
		}

		Vector2 commonPosition = pos + new Vector2 (1, 4);
		DrawRandomLayer (LayerType.Common, commonPosition);
	}

	public void ClearRooms() {
		foreach (Transform t in transform) {
			Destroy (t.gameObject);
		}
	}
}
